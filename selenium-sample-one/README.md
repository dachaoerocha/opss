**selenium-sample-one**

Selenium Java example that does a login to LinkedIn website and extracts some information from your profile.

**Getting Started for Windows (should work with other plattforms)**

1. You must have Java 8 (JRE at least) installed [Download here](https://java.com/en/download/)
2. You must have Chrome installed in your local machime ate the default location
3. [Dowanload Chrome Webdriver](http://chromedriver.chromium.org/downloads). Remember where you kept it, preferably on a short path (ex. C:\java\chromedriver.exe)
4. You can either clone the repository and build the project (requires Apache Maven) or you can download the binary [here](dist/selenium-sample-one-1.0-shaded.jar)
5. Run the binary file (ex. java -jar selenium-sample-one-1.0-shaded.jar)
6. Follow the instructions on the screen

---

## DCER Team