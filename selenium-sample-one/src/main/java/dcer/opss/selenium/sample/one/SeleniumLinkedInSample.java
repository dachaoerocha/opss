/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dcer.opss.selenium.sample.one;

import org.beryx.textio.TextIO;
import org.beryx.textio.TextIoFactory;
import org.beryx.textio.TextTerminal;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author seseso
 */
public class SeleniumLinkedInSample {
    
    private static void transitionsPagesWait(WebDriver d) {
        
        // Wait for a max time of 20 sec
        (new WebDriverWait(d, 30)).until(new ExpectedCondition<Boolean>() {
            
            @Override
            public Boolean apply(WebDriver d) {
                return true;
            }
        });
    }
    
    public static void main(String[] args) {
        
        TextIO textIO = TextIoFactory.getTextIO();

        String chromeDriver = textIO.newStringInputReader()
                .withDefaultValue("C:\\java\\chromedriver.exe")
                .read("Webdriver Chrome Driver Path");

        System.setProperty("webdriver.chrome.driver", chromeDriver);
        

        String user = textIO.newStringInputReader()
                .read("Username");

        String password = textIO.newStringInputReader()
                .withMinLength(6)
                .withInputMasking(true)
                .read("Password");
        
        // Create a new instance of the Chrome driver
        // Notice that the remainder of the code relies on the interface, 
        // not the implementation.
        WebDriver d = new ChromeDriver();

        // And now use this to visit LinkedIn
        d.get("https://www.linkedin.com");
        
        // Wait timeout for the page request
        transitionsPagesWait(d);
        
        // Find by id and fill email text field with provided
        d.findElement(By.id("login-email")).sendKeys(user);
        
        // Find by id and fill password text field with provided
        d.findElement(By.id("login-password")).sendKeys(password);
        
        // Click on Sign In button
        d.findElement(By.id("login-submit")).click();
        
        // Wait timeout for the page submit
        transitionsPagesWait(d);
        
        // Find HTML element by xpath
        WebElement el = d.findElement(By.xpath("//a[@data-control-name='identity_welcome_message']"));
        String name = el.getText();
                                                   
        TextTerminal terminal = textIO.getTextTerminal();
        terminal.printf("\nYour name in LinkedIn is %s.\n",
                name);
        
//        if(textIO.newBooleanInputReader()
//                .withDefaultValue(Boolean.TRUE)
//                .read("Exit this program?")) {
//            
//            d.quit();
//            textIO.dispose();
//        }
        
        el.click();
        
        WebDriverWait wait = new WebDriverWait(d, 20);
        WebElement headlineElement = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h2[class *= 'pv-top-card-section__headline']"))
        );
        
        String headline = headlineElement.getText();
        
        terminal.printf("\nYour headline in LinkedIn is %s.\n",
                headline);
        
        if(textIO.newBooleanInputReader()
                .withDefaultValue(Boolean.TRUE)
                .read("Exit this program?")) {
            
            d.quit();
            textIO.dispose();
            return;
        }
        
        terminal.printf("\nThe program has ended anyway... bye!\n");
        d.quit();
        textIO.dispose();
    }
}
